﻿#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"

using mt::math::Mat22d;
using mt::math::Vec2d;



int main()
{
	mt::math::Matrix <int, 3, 3> C({ {
		{1,2,3},
		{1,2,3},
		{1,2,3}
	} });

	mt::math::Matrix <int, 3, 4> Err;

	Mat22d A({ {
		 {1,2},
		 {3,4}
	} });

	Mat22d B({ {
		{2, 2},
		{3, 2}
	} });

	std::cout << "=== Test 1 ===" << std::endl;
	
	Mat22d TestN1 = A - B;
	std::cout << TestN1;
	
	std::cout << "=== Test 2 ===" << std::endl;

	Mat22d TestN2 = A * A;
	std::cout << TestN2;

	std::cout << "=== Test 3 ===" << std::endl;

	int ADet = A.det();

	std::cout << ADet << std::endl;

	std::cout << "=== Test 4 ===" << std::endl;
	
	std::cout << C.det() << std::endl;

	std::cout << "=== Test 5 ===" << std::endl;

	std::cout << Err.det() << std::endl;

	std::cout << "=== Test 6 ===" << std::endl;

	A.transp();
	C.transp();
	std::cout << A << std::endl;
	std::cout << C<< std::endl;

	return 0;
}