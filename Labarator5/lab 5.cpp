﻿//Дана последовательность операций «добавить вершину», 
//«удалить вершину» и «найти ключ» над бинарным деревом поиска. 
//Выполнить эти операции.

#include <iostream>
#include <fstream>
#include <string>

struct Elem
{
    int data;

    Elem* left;
    Elem* right;
    Elem* parent;
};
void CLEAR(Elem*& v)
{
    if (v == nullptr)
        return;
    // 

    CLEAR(v->left);
    // 

    CLEAR(v->right);

    // 
    delete v;
    v = nullptr;
}


Elem* MAKE(int data, Elem* p)
{
    Elem* q = new Elem;
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
};

void ADD(int data, Elem*& root)
{
    if (root == nullptr)
    {
        root = MAKE(data, nullptr);
        return;
    }
    Elem* v = root;
    while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
        if (data < v->data)
            v = v->left;
        else
            v = v->right;
    if (data == v->data)
        return;
    Elem* u = MAKE(data, v);
    if (data < v->data)
        v->left = u;
    else
        v->right = u;
};

Elem* SEARCH(int data, Elem* v)
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}

void DELETE(int data, Elem*& root)
{ 
    Elem* u = SEARCH(data, root);
    if (u == nullptr)
        return;
    if (u->left == nullptr && u->right == nullptr && u == root)
    {
        delete root;
        root = nullptr;
        return;
    }
    if (u->left == nullptr && u->right != nullptr && u == root) // TODO  u==root
    {
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    } 
    if (u->left != nullptr && u->right == nullptr && u == root) // TODO  u==root
    {
        Elem* t = u->left;
        while (t->right != nullptr)
            t = t->right;
        u->data = t->data;
        u = t;
    }
    if (u->left != nullptr && u->right != nullptr)
    {
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }
    Elem* t;
    if (u->left == nullptr)
        t = u->right;
    else
        t = u->left;
    if (u->parent->left == u)
        u->parent->left = t;
    else
        u->parent->right = t;
    if (t != nullptr)
        t->parent = u->parent;
    delete u;
}



int SEARCH_DEEP(int data, Elem*& v, int deep)
{
    if (v == nullptr)
        return INT_MIN;

    if (v->data == data)
        return(deep);

    if (data < v->data)
        return SEARCH_DEEP(data, v->left,deep+1);
    else
        return SEARCH_DEEP(data, v->right,deep+1);
   

}

int main()
{
        std::string line;
    char cmd;
    std::ifstream in("input.txt"); 
    std::ofstream out;
    out.open("output.txt");

    Elem* root = nullptr;

    if (in.is_open())
    {
        while (getline(in,line))
        {
            cmd = line[0];
            if (cmd == 'E')
                break;
            line.erase(0,1);
            int count = std::stoi(line);
            switch (cmd)
            {
            case '+': ADD(count, root); break;
            case '-': DELETE(count, root); break;
            case '?':                 
                if (SEARCH_DEEP(count,root, 1) == INT_MIN)
                    out << 'n';
                else
                    out << SEARCH_DEEP(count, root,1);                
                break;
            default: return -2;
            }   
        }
    }
    else
        return -1;
    CLEAR(root);
    in.close();     
    out.close();    
    return 0;
}

