﻿// Вариант 6.
//Подобрать структуру для хранения данных, 
//над которой преимущественно будут осуществляться операции удаления и добавления.Минимально – поиска.

//Т.к над данными минимально будет осуществляться операция поиска, то нет необходимости использовать бин. дерево .
//А для удаления и добавления отлично подходит односвязный список.

#include <iostream>

struct T_List
{
	T_List* next;
	int x;
};

void add(T_List*& head, int x)
{
	T_List* p = new T_List;
	p->x = x;

	p->next = head->next;
	head->next = p;
}
void print(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->x << std::endl;
		p = p->next;
	}
}

void clear(T_List* head)
{
	T_List* temp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		temp = p;
		p = p->next;
		delete temp;
	}
}

void DELETE_X(T_List* head,int n)

{

	T_List* tmp;

	T_List* p = head;

	while (p->next != nullptr)

	{

		if (p->next->x == n)

		{

			tmp = p->next;

			p->next = p->next->next;

			delete tmp;

		}

		else

			p = p->next;

	}

}




int main()
{
	T_List* list = new T_List;
	list->next = nullptr;

	add(list, 5);
	add(list, 4);
	add(list, 3);
	add(list, 2);

	print(list);

	DELETE_X(list, 5);

	std::cout << std::endl;

	print(list);
	
	clear(list);

	return 0;
}


