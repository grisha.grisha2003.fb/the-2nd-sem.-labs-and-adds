﻿#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"

using mt::math::Mat22d;
using mt::math::Vec2d;



int main()
{
	setlocale(LC_ALL, "Rus");

	mt::math::Matrix <double, 3, 3> C({ {
		{1,1,1},
		{1,2,3},
		{1,3,6}
	} });

	mt::math::Matrix <int, 3, 4> Err;

	Mat22d A({ {
		 {1,2},
		 {3,5}
	} });

	Mat22d B({ {
		{2, 2},
		{3, 2}
	} });

	std::cout << "=== Test 1 ===" << std::endl;
	
	Mat22d TestN1 = A - B;
	std::cout << TestN1;
	
	std::cout << "=== Test 2 ===" << std::endl;

	Mat22d TestN2 = A * A;
	std::cout << TestN2;

	std::cout << "=== Test 3 ===" << std::endl;

	int ADet = A.det<int>();

	std::cout << ADet << std::endl;

	std::cout << "=== Test 4 ===" << std::endl;
	
	std::cout << C.det<int>() << std::endl;

	std::cout << "=== Test 5 ===" << std::endl;

	std::cout << Err.det<int>() << std::endl;

	std::cout << "=== Test 6 ===" << std::endl;

	A.transp<int>();
	C.transp<int>(); //Работает, но я подобрал другую матрицу для теста №7
	std::cout << A << std::endl;
	std::cout << C << std::endl;

	std::cout << "=== Test 7 ===" << std::endl;
	
	Mat22d A2 = A.inv();
	std::cout << A2 << std::endl;

	mt::math::Matrix <double, 3, 3> C2 = C.inv();
	std::cout << C2 << std::endl;

	std::cout << A * A2 << std::endl;
	std::cout << C * C2 << std::endl;


	return 0;
}