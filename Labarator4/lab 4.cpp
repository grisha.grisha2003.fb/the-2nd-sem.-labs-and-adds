﻿//Вариант 6.
//Создайте односвязный список из N = 10000 целых чисел и реализуйте операцию поиска в нем элемента k.
//Выполните M = 1000 поисковых запросов.
//Реализуйте такую же операцию для массива.Сравните производительность.


#include <iostream>
#include <random>
#include <chrono>
#define N 10000
#define M 1000


class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};



struct T_List
{
	T_List* next;
	int x;
};

void add(T_List*& head, int x)
{
	T_List* p = new T_List;
	p->x = x;

	p->next = head->next;
	head->next = p;
}
void print(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->x << std::endl;
		p = p->next;
	}
}

void clear(T_List* head)
{
	T_List* temp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		temp = p;
		p = p->next;
		delete temp;
	}
}

int main()
{
	std::cout << "Enter search num...";
	int x;
	std::cin >> x;

	// Поиск в односвязном списке\\\\\\\\

	

	//Создание и заполнение списка\\\\\\\\\

	T_List* head = new T_List;
	head->next = nullptr;
	
	for (int i = 0; i < N; i++)
		add(head, rand()%100);

	// Поиск \\\\\\\\\\\

	Timer timer; // Начало отсчёта

	for (int i = 0; i < M; i++)
	{
		bool flag = true;
		T_List* p = head->next;
		while (p != nullptr)
		{	
			int j = 1;
			if (p->x == x)
			{
				std::cout << p->x << " pos " << j << std::endl;
				flag = false;
				break;
			}
			p = p->next;
			j++;
		}
		if (flag)
			std::cout << "there is no " << x << std::endl;
	}
	std::cout << std::endl << timer.elapsed() << std::endl; // Конец отсчёта

	clear(head);
	

	// Поиск в массиве \\\\\\\\\\

	/*

	// Создание массива

	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand()% 100;

	//Поиск

	Timer timer; // Начало отсчёта

	for (int i = 0; i < M; i++)
	{
		bool flag = true;
		for (int j = 0; j < N; j++)
		{
			if (mas[j] == x)
			{
				std::cout << x << " pos " << j << std::endl;
				flag = false;
				break;
			}
		}
		if(flag)
			std::cout << "there is no " << x << std::endl;
	}

	std::cout << std::endl << timer.elapsed() << std::endl; // Конец отсчёта

	*/

	return 0;
}

/*  Таким образом:
* 
*  При х = 50 время работы:
* 
* односвязный список - 0.568328
* 
* массив - 0.564502
* 
*  При х = 100(не существует) время работы:
* 
* односязный список - 0.555084
* 
* массив - 0.557888
* 
* 
* таким образом можно сделать вывод, что время поиска элемента в односвязном списке такое же, как и в массиве, 
* однако в массиве можно использовать бинарный поиск, чего нельзя сделать со списком (т.к нельзя сразу обращаться к отдельным элементам списка). 
* Следовательно, в этом случае массив эффективнее

*/