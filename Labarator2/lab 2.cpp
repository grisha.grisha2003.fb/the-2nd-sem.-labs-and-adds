﻿//Вариант 6.
//Дана строка длины N.Если в ней есть хотя бы одна гласная, отсортируйте буквы по алфавиту.


#include <iostream>
#include <string>
#include <Windows.h>
#include <chrono>
#include <random>

class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


std::string SwapSortStringABC(std::string& string)
{
    std::string alphabet = "аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ";
    int n;
    n = string.size();



    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i; j < n; j++)                          
        {
            int jsym;
            for (int j2 = 0; j2 < alphabet.size(); j2++)     
                if (string[j] == alphabet[j2])               
                {                                            
                    jsym = j2;                               
                    break;                                                   
                }                                            
                                                             
            int isym;                                        
            for (int i2 = 0; i2 < alphabet.size(); i2++)     
                if (string[i] == alphabet[i2])               
                {
                    isym = i2;
                    break;
                }

            if (isym > jsym)                                   
            {
                int voidChar;
                voidChar = string[i];
                string[i] = string[j];
                string[j] = voidChar;
            }
        }
    }
    return string;                                           
}


std::string BubbleSortStringABC(std::string& string)
{
    std::string alphabet = "аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ";
    int n;
    n = string.size();

    for (int i = 0; i < n-1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            int jsym;
            for (int j2 = 0; j2 < alphabet.size(); j2++)
                if (string[j] == alphabet[j2])
                {
                    jsym = j2;
                    break;
                }

            int isym;
            for (int i2 = 0; i2 < alphabet.size(); i2++)
                if (string[j+1] == alphabet[i2])
                {
                    isym = i2;
                    break;
                }

            if (isym < jsym)
            {
                char voidChar;
                voidChar = string[j];
                string[j] = string[j+1];
                string[j+1] = voidChar;
            }
        }
    }
    return string;
}

int GetNOS(char x) //Ф-я поиска эквивалентного числа для символа
{
    std::string alphabet = ".аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ";
    int NOS;
    for (int i = 0; i < alphabet.size(); i++)
        if (x == alphabet[i])
        {
            NOS = i;
            return NOS;
        }

 
}

std::string QuickStringSortABC(int a, int b, std::string string)
{
    if (a >= b)
       return string;
 

    int rd = rand() % 50;
    int min1 = min(GetNOS(string[a]), GetNOS(string[b]));
    int min2 = min(GetNOS(string[b]), rd);
    int min3 = min(GetNOS(string[a]), rd);
    int k = max( min1, min2, min3);

    int l = a - 1;
    int r = b + 1;
    while (true)
    {
        l = l + 1;
        while (GetNOS(string[l]) < k)
            l = l + 1;
        r = r - 1; 
        while (GetNOS(string[r]) > k)
            r = r - 1;
        if (l >= r)
            break;
        
         int temp = string[l];
        string[l] = string[r];
        string[r] = temp; 
    }
    r = l;
    l = l - 1;
    string = QuickStringSortABC(a, l, string);
    string = QuickStringSortABC(r, b, string);
    return string;
}




int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "Rus");
    std::string glas = "аАуУоОиИэЭыЫ";
    std::string string;
    std::cout << "Введите строку: ";
    std::cin >> string;
    int n;
    n = string.size();

    bool IsGlas = false;

    Timer timer; //Начало отсчёта

    

    for (int i = 0; i < n; i++)
        for (int j = 0; j < glas.size(); j++)
        {
            if (string[i] == glas[j])
                IsGlas = true;
        }


    if (IsGlas)
    {
        //Выбрать необходимое:

        //string = SwapSortStringABC(string);
        //string = BubbleSortStringABC(string);
        string = QuickStringSortABC(0, n - 1, string);


        std::cout << timer.elapsed() << std::endl; //Конец отсчёта

        std::cout << std::endl << string;
    }
    else
        std::cout << std::endl << "В строке нет гласных";


    return 0;
}
