﻿// Вариант 6.
// Дано слово длины N < 100. Получить из него слово, в котором буквы записаны наоборот.

#include <iostream>
#include <string>
#include <Windows.h>

void Function(std::string& string,int i, int n)
{
    if (n >= i)
    {
        char temp = string[n];
        string[n] = string[i];
        string[i] = temp;
        Function(string, i + 1, n - 1);
    }       
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "Rus");

    std::cout << "Введите строку" << std::endl;
    std::string string;
    std::cin >> string;
    Function(string, 0, string.size() - 1);

    std::cout << string;
    return 0;
}

