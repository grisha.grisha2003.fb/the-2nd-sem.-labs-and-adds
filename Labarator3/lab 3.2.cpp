﻿// lab 3.2
//Найдите корень уравнения ln x – 1 = 0 с точностью до 10^-4 с помощью бисекции


#include <iostream>

float func(float x)
{
    if (x < 0)
    {
        std::cout << "Х не может быть меньше 0, х принято за 1." << std::endl;
        x = 1;
    }
    return std::log(x) - 1;
}

float bisection(float a, float b)
{
    float x;
    float eps = 0.0001;
    float fa = func(a);
    while (true) 
    {
        x = (a + b) / 2;

        if ((abs(x - a) < eps) or (abs(x - b) < eps))
            return x;
        else if (func(x) * fa > 0)
            a = x;
        else
            b = x;
    }
 }

int main()
{
    setlocale(LC_ALL, "rus");
    std::cout << bisection(-100, 100);
    return 0;
}

