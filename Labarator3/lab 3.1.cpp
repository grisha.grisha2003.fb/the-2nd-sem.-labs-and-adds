﻿// lab 3.cpp 
//1.	Реализуйте алгоритм бинарного поиска, о котором шла речь на лекции. 
//Решите задачу выполнения M (0<M<10000) поисковых запросов над массивом из N (0<N<100000) элементов,
//используя проход по массиву циклом и бинарный поиск. Сравните время работы этих двух подходов.

#include <iostream>
#include <chrono>
#include <random>
#include <vector>

class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


void BinSearch(int b, int targ, std::vector<int> &mas)
{
    if (targ < mas[0])
        std::cout << "Er.404";
    if (targ == mas[0])
        std::cout << "Finded. pos " << 1;
    if (targ > mas[b])
        std::cout << "Er.404";
    int a = 0;
    int n = b;

    while ((a + 1) < n)
    {
        int c = (a + n) / 2 ;
        
        if (targ > mas[c])
            a = c;
        else
            n = c;
    }

    if (mas[n] == targ)
        std::cout << "Finded. pos " << n+1;
    else if (mas[a] == targ )
            std::cout << "Finded. pos " << a+1;
    else
        std::cout << "Er.404";
}

void StiupidSearch(int targ, std::vector<int> &mas)
{
    bool flag = true;
    for (int i = 0; i < mas.size() - 1; i++)
    {
        
        if (targ == mas[i])
        {
            std::cout << "Finded. pos " << i+1;
            flag = false;
            break;
        }
    }
    if (flag)
        std::cout << "Er.404";
}




std::vector<int> QuickSort(int a, int b,std::vector<int> mas)
{
    if (a >= b)
        return mas;

    int rd = rand() % 100;
    int min1 = std::min(mas[a], mas[b]);
    int min2 = std::min(mas[b], rd);
    int min3 = std::min(mas[a], rd);
    int k = std::max(std::max(min1, min2), min3);

    int l = a - 1;
    int r = b + 1;
    while (true)
    {
        l = l + 1;
        while (mas[l] < k)
            l = l + 1;
        r = r - 1;
        while (mas[r] > k)
            r = r - 1;
        if (l >= r)
            break;

        int temp = mas[l];
        mas[l] = mas[r];
        mas[r] = temp;
    }
    r = l;
    l = l - 1;
    mas = QuickSort(a, l, mas);
    mas = QuickSort(r, b, mas);


    return mas;

}



int main()
{
    setlocale(LC_ALL, "Rus");
    std::vector<int> mas;

    int N;
    std::cout << "Введите число элементов массива - ";
    std::cin >> N;

    //Заполнение массива случайными числами
    for (int i = 0; i < N; i++)
        mas.push_back(rand() % 100);

    int M;
    std::cout << "Введите число запросов - ";
    std::cin >> M;

    int targ;
    std::cout << "Введите искомое число - ";
    std::cin >> targ;

    
    Timer timer; // Начало отсчёта
    mas = QuickSort(0, N - 1, mas);
    for (int m = 0; m < M; m++)
    {
        //Выбрать нужное
                
        //BinSearch(N - 1, targ, mas);
                
        StiupidSearch(targ, mas);
        
    }
    std::cout << std::endl <<  timer.elapsed() << std::endl; // Конец отсчёта
    return 0;
}

/*Вывод:
 при M = 1000 и N = 1000 поиск числа 50

 Бинарный поиск: 0.506048
 Цикличный поиск: 0.528021
 
 при М = 10000 и N = 10000 поиск числа 50

 Бинарный поиск: 5.89921 (я считаю большее количество времени занял вывод в консоль)
 Цикличный поиск: 8.42695

 при M = 1 и N = 10000 разница минимальная ( ~0.001 c) 

 при M = 10000 и N = 100000 поиск числа 50

 Бинарный поиск: 15.1858
 Цикличный поиск: 41.722

*/
