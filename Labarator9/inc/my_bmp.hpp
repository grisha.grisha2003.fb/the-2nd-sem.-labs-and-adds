#pragma once
#pragma once
#include <iostream>
#include <fstream>
#include "inc/structs.hpp"

namespace mbmp
{
    class BMP_IMAGE
    {
    public:

        // �����������
        BMP_IMAGE()
        {
            std::cout << "Constructor" << std::endl;
        }

        // ����������� �����������
        BMP_IMAGE(const BMP_IMAGE& img)
        {
     
            m_BMPHEADER = img.m_BMPHEADER;
            m_BMPINFO = img.m_BMPINFO;

            m_Pixels = new Pixel * [m_BMPINFO.Height];
            for (int i = 0; i < m_BMPINFO.Height; i++)
                m_Pixels[i] = new Pixel[m_BMPINFO.Width];

            for (int i = 0; i < m_BMPINFO.Height; i++)
            {
                for (int j = 0; j < m_BMPINFO.Width; j++)
                    m_Pixels[i][j] = img.m_Pixels[i][j];
            }
        }

        BMP_IMAGE& operator=(const BMP_IMAGE& img)
        {
            m_BMPHEADER = img.m_BMPHEADER;
            m_BMPINFO = img.m_BMPINFO;

            m_Pixels = new Pixel * [m_BMPINFO.Height];
            for (int i = 0; i < m_BMPINFO.Height; i++)
                m_Pixels[i] = new Pixel[m_BMPINFO.Width];

            for (int i = 0; i < m_BMPINFO.Height; i++)
            {
                for (int j = 0; j < m_BMPINFO.Width; j++)
                    m_Pixels[i][j] = img.m_Pixels[i][j];
            }
            return *this;
        }

        // ����������
        ~BMP_IMAGE()
        {
            std::cout << "Destructor" << std::endl;
            for (int i = 0; i < m_BMPINFO.Height; i++)
                delete[] m_Pixels[i];
            delete[] m_Pixels;
        }


        // ���� ����������� 
        void input(const char* filename)
        {
            std::ifstream in(filename, std::ios::binary);
            // ������� 14 ������ ��������� � ��������� ��������� BMPHEADER
            in.read(reinterpret_cast<char*>(&m_BMPHEADER), sizeof(BMPHEADER));

            in.read(reinterpret_cast<char*>(&m_BMPINFO), sizeof(BMPINFO));

            m_Pixels = new Pixel * [m_BMPINFO.Height];
            for (int i = 0; i < m_BMPINFO.Height; i++)
                m_Pixels[i] = new Pixel[m_BMPINFO.Width];

            for (int i = 0; i < m_BMPINFO.Height; i++)
            {
                for (int j = 0; j < m_BMPINFO.Width; j++)
                    in.read(reinterpret_cast<char*>(&m_Pixels[i][j]), sizeof(Pixel));

                if ((3 * m_BMPINFO.Width) % 4 != 0)
                    for (int j = 0; j < 4 - (3 * m_BMPINFO.Width) % 4; j++)
                    {
                        char c;
                        in.read(&c, 1);
                    }
            }
        }

        // ����� �����������
        void output(const char* filename)
        {
            std::ofstream out(filename, std::ios::binary);

            int width = m_BMPINFO.Width;
            int height = m_BMPINFO.Height;

            // ������������ ���������
            BMPHEADER bmpHeader_new;
            bmpHeader_new.Type = 0x4D42; // ��� ������ BMP
            bmpHeader_new.Size = 14 + 40 + (3 * width * height);
            if (width % 4 != 0)
                bmpHeader_new.Size += (4 - (3 * width) % 4) * height;
            bmpHeader_new.OffBits = 54;
            bmpHeader_new.Reserved1 = 0;
            bmpHeader_new.Reserved2 = 0;

            out.write(reinterpret_cast<char*>(&bmpHeader_new), sizeof(BMPHEADER));

            // ������������ ���������� �� �����������
            BMPINFO bmpInfo_new;
            bmpInfo_new.BitCount = 24;
            bmpInfo_new.ClrImportant = 0;
            bmpInfo_new.ClrUsed = 0;
            bmpInfo_new.Compression = 0;
            bmpInfo_new.Height = height;
            bmpInfo_new.Planes = 1;
            bmpInfo_new.Size = 40;
            bmpInfo_new.SizeImage = bmpHeader_new.Size - 54;
            bmpInfo_new.Width = width;
            bmpInfo_new.XPelsPerMeter = 0;
            bmpInfo_new.YPelsPerMeter = 0;

            out.write(reinterpret_cast<char*>(&bmpInfo_new), sizeof(BMPINFO));

            // �������� �������
            for (int i = 0; i < bmpInfo_new.Height; i++)
            {
                for (int j = 0; j < bmpInfo_new.Width; j++)
                    out.write(reinterpret_cast<char*>(&m_Pixels[i][j]), sizeof(Pixel));

                if ((3 * bmpInfo_new.Width) % 4 != 0)
                    for (int j = 0; j < 4 - (3 * bmpInfo_new.Width) % 4; j++)
                    {
                        char c = 0;
                        out.write(&c, 1);
                    }
            }
        }

        void black_white_filtre()
        {
            for (int i = 0; i < m_BMPINFO.Height; i++)
                for (int j = 0; j < m_BMPINFO.Width; j++)
                {
                    unsigned char bgr = (m_Pixels[i][j].b + m_Pixels[i][j].g + m_Pixels[i][j].r) / 3;
                    m_Pixels[i][j].b = m_Pixels[i][j].g = m_Pixels[i][j].r = bgr;
                }
        }


    private:

        BMPHEADER m_BMPHEADER;
        BMPINFO m_BMPINFO;
        Pixel** m_Pixels;
    };
}