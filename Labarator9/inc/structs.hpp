#pragma once
#include <iostream>

#pragma pack(1) // ��������� ������������
struct BMPHEADER
{
    unsigned short    Type = NULL;
    unsigned int      Size = NULL;
    unsigned short    Reserved1 = NULL;
    unsigned short    Reserved2 = NULL;
    unsigned int      OffBits = NULL;
};
#pragma pack()

#pragma pack(1)
struct BMPINFO
{
    unsigned int    Size = NULL;
    int             Width = NULL;
    int             Height = NULL;
    unsigned short  Planes = NULL;
    unsigned short  BitCount = NULL;
    unsigned int    Compression = NULL;
    unsigned int    SizeImage = NULL;
    int             XPelsPerMeter = NULL;
    int             YPelsPerMeter = NULL;
    unsigned int    ClrUsed = NULL;
    unsigned int    ClrImportant = NULL;

};
#pragma pack()

#pragma pack(1)
struct Pixel
{
    unsigned char b = NULL;
    unsigned char g = NULL;
    unsigned char r = NULL;

};
#pragma pack()