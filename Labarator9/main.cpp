﻿#include <iostream>
#include <fstream>
#include "inc/my_bmp.hpp"


#pragma pack(1) // Отключить выравнивание


int main()
{
    setlocale(LC_ALL, "Rus");
    mbmp::BMP_IMAGE Image;
    mbmp::BMP_IMAGE Image2;
    
    std::cout << "Тест 1: Ввод и вывод одного изображения. Вывод под названием test1.bmp" << std::endl;

    Image.input("in.bmp");
    Image.output("test1.bmp");

    std::cout << "Тест 2: Ввод 2х изображений. Присваивание первому изображению второе. Вывод под названием test2.bmp " << std::endl;

    Image2.input("img2.bmp");
    Image = Image2;
    Image.output("test2.bmp");

    std::cout << "Тест 3: Недо-чёрно-белый фильтр. Вывод под названием test3.bmp  " << std::endl;

    Image.input("in.bmp");
    Image.black_white_filtre();
    Image.output("test3.bmp");

}